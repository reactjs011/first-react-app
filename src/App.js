import "./App.css";
import {} from "react-bootstrap";
import Home from "./pages/Home";
import AppFeatures from "./components/AppFeatures";
import AppStarter from "./components/AppStarter";
import AppCarousels from "./components/AppCarousels";
function App() {
  return (
    <>
      <Home />
      <AppCarousels />
      <AppFeatures />
      <AppStarter />
    </>
  );
}

export default App;
